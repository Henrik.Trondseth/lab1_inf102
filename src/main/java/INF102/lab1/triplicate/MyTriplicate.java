package INF102.lab1.triplicate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        List<T> newList = new ArrayList<T>();
        for(T t : list){
            newList.add(t);
        }
        Collections.sort(newList, Collections.reverseOrder());
        int n = list.size();
        for(int i = 2; i < n; i++){
            if(newList.get(i).equals(newList.get(i-1)) && newList.get(i).equals(newList.get(i-2))){
                return newList.get(i);
            }
        }
        return null;
    }
    
}
